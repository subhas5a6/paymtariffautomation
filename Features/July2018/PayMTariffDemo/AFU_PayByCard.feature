Feature: AFU_PayByCard

  #launch hooks and get browser
  @Web
  Scenario Outline: Successful completion of a PAY M dongle Acqusistion Journey
    Given I login to Agent shop
    And performs Upgrade for <user>
    And Select valid <Tariffs> from tariffs tab
    And Select a valid PayM <Device>
    And select a valid Handset and Tariff combination
    And Validate all the Basket content and checkout
    And Choose Business preferences <B1> <B2> <B3> <B4> and Channel Preferences <Text> <Email> <Phone> <Post> for <consumer> when GDPR <status> <DeviceType> for AFU journey
    Then perform all the advisory checks
    When Pay by card
    Then Order confirmation message should be displayed

    Examples:
      | user        | Device | Tariffs                                                 | consumer | B1     | B2  | B3     | B4  | Text | Email  | Phone | Post | status  | DeviceType |
      | 07729000526 | Random | T:CR7507:UnlimitedMins:24M:6GB:GBP24:S1:Primary:FLX:CCA | Me       | Select | Not | Select | Not | Not  | Select | Not   | Not  | Enabled | Connected  |