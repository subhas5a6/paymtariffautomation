Feature: AFU_SubmitOrder

  #launch hooks and get browser
  @Web
  Scenario Outline: Successful completion of a PAY M dongle Acquisition Journey
    Given I login to Agent shop
    And performs Upgrade for <user>
    And Select valid <Tariffs> from tariffs tab
    And Select a valid PayM <Device>
    And select a valid Handset and Tariff combination
    And Validate all the Basket content and checkout
    And Choose Business preferences <B1> <B2> <B3> <B4> and Channel Preferences <Text> <Email> <Phone> <Post> for <consumer> when GDPR <status> <DeviceType> for AFU journey
    Then perform all the advisory checks
    When submit order button is clicked
    Then Order confirmation message should be displayed

    Examples:
      | user        | Device | Tariffs                                      | consumer | B1     | B2  | B3  | B4  | Text | Email  | Phone | Post   | status  | DeviceType |
      | 07521128090 | Random | T:CR7507:UnlimitedMins:12M:SIMO:2GB:GBP14:S8 | Me       | Select | Not | Not | Not | Not  | Select | Not   | Select | Enabled | Connected  |
