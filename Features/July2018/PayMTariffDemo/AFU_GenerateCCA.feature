Feature: AFU_GenerateCCA

  #launch hooks and get browser
  @Web
  Scenario Outline: AFU_GenerateCCA
    Given I login to Agent shop
    And performs Upgrade for <User>
    And Select valid <Tariffs> from tariffs tab
    And Select a valid PayM <Device>
    And select a valid Handset and Tariff combination_new
    And Validate all the Basket content and checkout
    And Choose Business preferences <B1> <B2> <B3> <B4> and Channel Preferences <Text> <Email> <Phone> <Post> for <consumer> when GDPR <status> <DeviceType> for AFU journey
    Then perform all the advisory checks_new
    And Click on 'Generate CCA' button
    And click on the 'CCA' link

    Examples:
      | User        | Device | Tariffs                                                  | consumer | B1  | B2  | B3     | B4  | Text   | Email  | Phone  | Post   | status  | DeviceType |
      | 07568415205 | Random | T:CR7507:UnlimitedMins:24M:20GB:GBP30:S3:Primary:FLX:CCA | Me       | Not | Not | Select | Not | Select | Select | Select | Select | Enabled | Connected  |