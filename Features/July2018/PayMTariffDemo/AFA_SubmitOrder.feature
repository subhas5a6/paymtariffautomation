Feature: AFA_SubmitOrder
  This scenario ensures that when agent on acquisition journey selects 'SIMO' and then should be able to place an order successfully

  @Web
  Scenario Outline: AFA_SubmitOrder
    Given I login to Agent shop
    And performs Acquisition for New user
    And Select valid <Tariff> from tariffs tab
    And Validate all the Basket content and checkout
    Then perform all the advisory checks
    And perform the credit checks using valid <Firstname>, <Surname>, <HouseNumber>, <PostCode> and valid <Username>
    And Register customer with valid <Password>, <confirmPassword>, <SecurityAnswer> in delivery page
    And validate register status
    And Choose Business preferences <B1> <B2> <B3> <B4> and Channel Preferences <Text> <Email> <Phone> <Post> for <Consumer> when GDPR <status> <DeviceType> <DeviceModule> for AFA journey
    And Choose <DeliveryType> delivery address and delivery time
    When submit order button is clicked
    Then agent should be displayed with updated copy of 'Refer with Simo' response

    Examples:
      | Tariff                                                   | DeliveryType | Firstname | Surname | Username     | Password | confirmPassword | SecurityAnswer | HouseNumber | PostCode | B1     | B2  | B3     | B4  | Text   | Email  | Phone | Post | Consumer | status  | DeviceType | DeviceModule |
      | T:CR7507:UnlimitedMins:15GB:GBP22:S4:Primary:FLX:30D:CCA | HomeDelivery | TEST      | Accepta | Test Accepta | test1234 | test1234        | Jamal          | 6           | SL11ER   | Select | Not | Select | Not | Select | Select | Not   | Not  | Me       | Enabled | Connected  | Simo         |