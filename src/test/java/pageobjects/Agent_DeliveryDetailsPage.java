package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Agent_DeliveryDetailsPage {
	

		
		@FindBy(how=How.ID,using="accountName")
		public static WebElement AccountName;
		
		@FindBy(how=How.ID,using="sortCode")
		public static WebElement SortCode;
		
		@FindBy(how=How.ID,using="accountNumber")
		public static WebElement AccountNumber;
		
		@FindBy(how=How.ID,using="agreeToCreditCheck")
		public static WebElement AgreeCreditCheck;
		
		@FindBy(how=How.ID,using="captureCardDetails")
		public static WebElement CardCapture;
		

		
}
