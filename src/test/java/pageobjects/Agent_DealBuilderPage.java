package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Agent_DealBuilderPage {

    @FindBy(how = How.ID, using = "plansTab")
    public static WebElement TariffsTab;

    @FindBy(how = How.ID, using = "devicesTab")
    public static WebElement DevicesTab;

    @FindBy(how = How.ID, using = "extrasTab")
    public static WebElement ExtrasTab;

    @FindBy(how = How.ID, using = "(//span[@class='boltonName'])[2]")
    public static WebElement SelectBolton;

    @FindBy(how = How.ID, using = "startCheckoutFromPrivateBasketButton")
    public static WebElement Checkout;

    @FindBy(how = How.XPATH, using = "//*[@id='deviceTable_filter']/label/input")
    public static WebElement SearchTextBox_PayMDevice;

    @FindBy(how = How.XPATH, using = "//*[@id='planTable_filter']/label/input")
    public static WebElement SearchTextBox_Tariff;

    @FindBy(how = How.XPATH, using = "//table[@id='deviceTable']/tbody/tr/td/a/img")
    public static WebElement SelectInStockPAYMDevice;

    @FindBy(how = How.XPATH, using = "//*[@id='planTable']/tbody/tr[1]/td[1]")
    public static WebElement SelectingFirstAvailableTariff;


    @FindBy(how = How.XPATH, using = "//*[@id='dataAllowances']/table/tbody/tr/td[1]/a")
    public static WebElement SelectingAvailableDataAllowance;

    @FindBy(how = How.XPATH, using = "//*[@class='priceSelection']/select")
    public static WebElement HandsetTariffCombination;

    @FindBy(how = How.XPATH, using = "//*[@id='buy-09c627bc-5e88-4d5d-a46d-4ad2e0b3dc22']/img ")
    public static WebElement SelectPromotion;


    //check check weather the promotions are displayed in Deal builder or not
    @FindBy(how = How.XPATH, using = "//table[@class='lineItemTable Promotions']")
    public static WebElement promotions_DealBuilder;

    @FindBy(how = How.XPATH, using = "html/body/div[1]/div/div[2]/div[1]/table/tbody/tr[1]/td[1]/a/img")
    public static WebElement firstAvailableDevice;

    @FindBy(how = How.XPATH, using = "//*[@id='deviceTable_filter']/label/input")
    public static WebElement DeviceSearchFilter;

    @FindBy(how = How.XPATH, using = "//div[@id='deviceTable_filter']/label/input")
    public static WebElement SearchDevice;

    ///////////////////////////// Trade In////////////////////////

    @FindBy(how = How.XPATH, using = "//*[@id='buyOutButton']")
    public static WebElement AgentBuyOut_Button;

    @FindBy(how = How.ID, using = "checkStoreStock")
    public static WebElement CheckStore;

    @FindBy(how = How.ID, using = "postcode")
    public static WebElement Postcode;

    @FindBy(how = How.ID, using = "findStores")
    public static WebElement searchStore;

    @FindBy(how = How.XPATH, using = "(//*[@class='selectStore'])[1]")
    public static WebElement selectStore;

    @FindBy(how = How.ID, using = "chosen-store-details")
    public static WebElement Storedetails;

    //check the device weather added into the Deal Builder
    @FindBy(how = How.XPATH, using = "//table[@class='lineItemTable device']//th")
    public static WebElement deviceAdded_DealBuilder;

    //check the Tariff weather added into the Deal Builder
    @FindBy(how = How.XPATH, using = "//table[@class='lineItemTable tariff']//th")
    public static WebElement tariffAdded_DealBuilder;


}
