package pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;



public class DeliveryPage {


    @FindBy(how = How.XPATH, using = "//input[@name='houseNum']")
    public static WebElement HouseNum;

    @FindBy(how = How.XPATH, using = "//*[@name='postcode']")
    public static WebElement Post;

    @FindBy(how = How.XPATH, using = "//*[@class='findAddressBtn']")
    public static WebElement FindAddress;

    @FindBy(how = How.XPATH, using = "(//*[@class='selectAddrBtn'])[1]")
    public static WebElement SelectAdd1;


}


