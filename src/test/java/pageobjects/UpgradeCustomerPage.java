package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class UpgradeCustomerPage {

	public static final String RecycleWidget = null;


	@FindBy(how = How.XPATH, using = "(//input[@id='username'])[2]")
	public static WebElement username;

	@FindBy(how = How.XPATH, using = "(//input[@id='password'])[2]")
	public static WebElement password;

	@FindBy(how = How.XPATH, using = "//input[@id='signInButton']")
	public static WebElement signInButton;

	}

