package actionsPerformed;

import GlobalActions.RandomEmailAddressCreation;
import GlobalActions.Screenshots;
import GlobalActions.scrollToAnElement;
import cucumber.api.DataTable;
import helpers.Environment;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import pageobjects.DeliveryPage;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DeliveryPageActions extends Environment {

    public List<HashMap<String, String>> datamap;
    final static Logger log = Logger.getLogger("DeliveryPageActions");
    static JavascriptExecutor js = (JavascriptExecutor) driver;

    public static void SetDelivery_AFU() throws InterruptedException {
        Thread.sleep(5000);
        try {


                pageobjects.DeliveryPage.HouseNum.sendKeys("12");
                log.debug("Entered House number");
                Thread.sleep(2000);
                pageobjects.DeliveryPage.Post.sendKeys("B15 2LG");
                log.debug("Entered Post code");
                Thread.sleep(2000);
                pageobjects.DeliveryPage.FindAddress.click();
                log.debug("Clicked on the Find address button");
                Thread.sleep(5000);


            if (driver.findElements(By.xpath("(//*[@class='selectAddrBtn'])[1]")).size() > 0) {
                pageobjects.DeliveryPage.SelectAdd1.click();
                log.debug("Selected an address");
            }
            Thread.sleep(3000);
            Screenshots.captureScreenshot();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
