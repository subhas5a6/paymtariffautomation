package actionsPerformed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import GlobalActions.CommonUtilities;
import GlobalActions.scrollToAnElement;
import junit.framework.AssertionFailedError;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.apache.commons.lang.StringUtils;
import com.google.common.base.Function;

import GlobalActions.Screenshots;
import helpers.Environment;
import helpers.Filereadingutility;
import helpers.setRuntimeProperty;
import pageobjects.*;
import steps.Hooks;

public class UpgradeCustomerPageActions extends Environment {

    static String RunTimeFilePath = System.getProperty("user.dir") + "\\Configurations\\Properties\\Run.properties";
    final static Logger log = Logger.getLogger("UpgradeCustomerPageActions");
    static JavascriptExecutor executor = (JavascriptExecutor) driver;
    static ArrayList<Integer> datalistafter = new ArrayList<Integer>();
    static int position = 0;
    static int PositionUpgrade = 0;

    public static void Login(String username, String password) throws InterruptedException, IOException {
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        pageobjects.UpgradeCustomerPage.username.sendKeys(username);
        pageobjects.UpgradeCustomerPage.password.sendKeys(password);

        Screenshots.captureScreenshot();
        //Screenshots.captureScreenshot(Hooks.directoryName);

        Thread.sleep(5000);
        if (pageobjects.UpgradeCustomerPage.signInButton.isDisplayed()) {
            pageobjects.UpgradeCustomerPage.signInButton.click();
            log.debug("Clicked on Signin button");
        }
        Thread.sleep(10000);

        log.debug("Title of the page is " + driver.getTitle());
        /*
         * if ((!driver.getTitle().
		 * contains("O2 | Accounts | Please verify your email address") ||
		 * !driver.getTitle().contains("O2 | Accounts | Update username"))) {
		 *
		 * Assert.fail("Login failed");
		 *
		 * } else {
		 *
		 * log.debug("Logged in successfully"); }
		 */
        /*
         * try { log.debug("Going to click on Continue link");
		 * pageobjects.UpgradeCustomerPage.Continue.click();
		 *
		 * } catch (Exception e) { // TODO Auto-generated catch block
		 * log.debug("Continue button is not there, it should be fine" );
		 *
		 * }
		 */


    }

}
