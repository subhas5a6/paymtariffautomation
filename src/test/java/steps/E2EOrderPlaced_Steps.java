package steps;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import GlobalActions.*;
import actionsPerformed.*;
import cucumber.api.DataTable;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import GlobalActions.Autoredirection;
import GlobalActions.CommonUtilities;
import GlobalActions.JuneReleaseValidations;
import GlobalActions.MouseHoverAction;


import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.Filereadingutility;
import org.testng.Assert;
import org.testng.asserts.Assertion;
import pageobjects.*;

public class E2EOrderPlaced_Steps {

    private static final String Filteroption = null;
    public WebDriver driver;
    public List<HashMap<String, String>> datamap;
    String FilterDataOption = null;
    LinkedList<String> expectedListBeforeSort = null;
    LinkedList<String> originalList = null;
    ArrayList<Integer> originalTariffList = null;
    ArrayList<Integer> expectedTariffListBeforeSort = null;
    LinkedList<String> TempList3 = null;
    String DataFilterRange = null;
    ArrayList<Integer> datalistbefore = new ArrayList<Integer>();
    ArrayList<Integer> datalistafter = new ArrayList<Integer>();
    ArrayList<Integer> monthlycostlistafter = new ArrayList<Integer>();
    ArrayList<Integer> upfrontcostlistafter = new ArrayList<Integer>();
    final static Logger log = Logger.getLogger("E2EOrderPlaced_Steps");
    static int BuyOutValue = 0;
    static int TradeInValue = 0;
    public static Hashtable selectedElements = new Hashtable();
    static String expPlnList;

    public E2EOrderPlaced_Steps() {
        driver = Hooks.driver;
        // datamap = DataReader.data();

    }

    /**
     * ############## Agent Journeys Demo ##############
     */


    // by JamalKhan

    @Given("^Validate Credit check status for ReferralwithSimo$")
    public void validate_Creditcheck_status_for_ReferralwithSimo() {
        try {
            driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_CreditCheckDetailsPage.class);
            Agent_CreditCheckPageActions.CreditcheckReferStatus();
            log.debug("Completed Credit check");

            Thread.sleep(5000);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to perform credit checks , please see the failure screenshot");
            Assert.fail("Unable to perform credit checks , please see the failure screenshot");

        }
    }


    @Given("^select a valid Handset and Tariff combination_new$")
    public void select_a_valid_Handset_and_Tariff_combination_new() {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Thread.sleep(7000);
            Agent_DealBuilderPageActions.HandsetTariffCombination_new();
            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to select valid tariff and handset combination");
        }
    }


    @Given("^I login to Agent shop$")
    public void LoginAgentShop() {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            String relativePath = System.getProperty("user.dir");
            String EnvPropFilePath = relativePath + "\\Configurations\\Properties\\AppConfig.properties";
            String Newurl = Filereadingutility.getPropertyValue(EnvPropFilePath, "AgentUrl");
            driver.navigate().to(Newurl);
            Agent_HomePagePageActions.ValidateAgentHomepage();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to Login/validate home page, please see the failure screenshot");
            Assert.fail("Unable to Login/validate home page, please see the failure screenshot");

        }

    }

    @Given("^select a valid Handset and Tariff combination$")
    public void select_a_valid_Handset_and_Tariff_combination() {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.HandsetTariffCombination();
            Thread.sleep(6000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to select valid tariff and handset combination");
        }
    }

    @Given("^Select valid ([^\"]*) from extras tab$")
    public void select_valid_Random_from_extras_tab(String Extras) {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.SelectExtras(Extras);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail("Unable to select extras");
        }
    }

    @Given("^performs Upgrade for ([^\"]*)$")
    public void performs_Upgrade(String msisdn) throws Throwable {
        // try {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PageFactory.initElements(driver, Agent_HomePage.class);
        Agent_HomePagePageActions.FindUser(msisdn);
        Thread.sleep(3000);
        Agent_HomePagePageActions.upgradeUser();
        Thread.sleep(4000);
        Screenshots.captureScreenshot();
        /*
         * } catch (Exception e) { // TODO Auto-generated catch block System.out.
         * println("Unable to login for upgrade for user in Agent shop, please see the failure screenshot"
         * ); Assert.
         * fail("Unable to login for upgrade for user in Agent shop, please see the failure screenshot"
         * );
         *
         * }
         */
    }

    @Given("^performs Acquisition for New user$")
    public void performs_Acquisition_for_New_user() {
        try {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_HomePage.class);
            Agent_HomePagePageActions.NewUser();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.out
                    .println("Unable to perform Acquistion for user in Agent shop, please see the failure screenshot");
            Assert.fail("Unable to perform Acquistion for user in Agent shop, please see the failure screenshot");

        }
    }

    @Given("^performs Agent Existing customer journey for ([^\"]*)$")
    public void performs_Agen_Existing_journey(String msisdn) {
        try {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_HomePage.class);
            Agent_HomePagePageActions.FindUser(msisdn);
            Thread.sleep(3000);
            Agent_HomePagePageActions.NewConnection();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.out
                    .println("Unable to perform Acquistion for user in Agent shop, please see the failure screenshot");
            Assert.fail("Unable to perform Acquistion for user in Agent shop, please see the failure screenshot");

        }
    }

    @Given("^Select a valid PayM ([^\"]*)")
    public void SelectValid_Device(String Device) {
        try {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Thread.sleep(7000);
            Agent_DealBuilderPageActions.SelectPAYMDevice(Device);
            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to select Valid device, please see the failure screenshot");
            Assert.fail("Unable to select Valid device, please see the failure screenshot");

        }

    }

    @Given("^Select valid ([^\"]*) from tariffs tab$")
    public void SelectTariff(String Tariff) {
        try {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Thread.sleep(7000);
            Agent_DealBuilderPageActions.SelectTariff(Tariff);
            // log.debug("Selecting a valid tariff");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to select valid tariff, please see the failure screenshot");
            Assert.fail("Unable to select valid tariff, please see the failure screenshot");

        }

    }

    @Given("^Validate all the Basket content and checkout$")
    public void validate_all_the_Basket_content_and_checkout() {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.ValdiateBasket();
            Thread.sleep(2000);
            Agent_DealBuilderPageActions.checkout();
            Thread.sleep(7000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to validate basket content/checkout , please see the failure screenshot");
            Assert.fail("Unable to validate basket content/checkout , please see the failure screenshot");

        }
    }

    @Then("^perform all the advisory checks$")
    public void advisory_checks() {
        try {
            //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            Thread.sleep(6000);
            PageFactory.initElements(driver, Agent_AdvisoryPage.class);
            PageFactory.initElements(driver, DeliveryPage.class);

            if (pageobjects.DeliveryPage.HouseNum.isDisplayed()) {
                DeliveryPageActions.SetDelivery_AFU();
            }
            Thread.sleep(4000);
            Agent_AdvisoryChecksActions.AgreeAdvsioryCheck();
            Thread.sleep(6000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to perform advisory checks , please see the failure screenshot");
            Assert.fail("Unable to perform advisory checks , please see the failure screenshot");

        }
    }

    @Then("^perform all the advisory checks_new$")
    public void advisory_checks_new() {
        try {
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_AdvisoryPage.class);
            Agent_AdvisoryChecksActions.AgreeAdvsioryCheck_new();
            Thread.sleep(6000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.out.println("Unable to perform advisory checks , please see the failure screenshot");
            Assert.fail("Unable to perform advisory checks , please see the failure screenshot");

        }
    }

    @Then("^perform the credit checks using valid ([^\"]*), ([^\"]*), ([^\"]*), ([^\"]*) and valid ([^\"]*)$")
    public void CreditCheck(String Firstname, String Surname, String HouseNumber, String PostCode, String Username) {
        try {
            driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_CreditCheckDetailsPage.class);
            Agent_CreditCheckPageActions.Creditcheck(Firstname, Surname, HouseNumber, PostCode);
            log.debug("Completed Credit check");
            Agent_CreditCheckPageActions.BankDetails(Username);
            log.debug("Completed Bank details");
            Thread.sleep(5000);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to perform credit checks , please see the failure screenshot");
            Assert.fail("Unable to perform credit checks , please see the failure screenshot");

        }

    }

    @Then("^Register the customer with valid ([^\"]*), ([^\"]*), ([^\"]*), ([^\"]*) and other valid details in delivery page_new$")
    public void register_the_customer_new(String Firstname, String Surname, String HouseNumber, String PostCode) {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Agent_RegisterCustomerActions.PayGRegistration_new(Firstname, Surname, HouseNumber, PostCode);

        } catch (Exception e) { // TODO Auto-generated catch block
            System.out.println("Unable to Register customer , please see the failure screenshot");
            Assert.fail("Unable to Register customer , please see the failure screenshot");

        }
    }


    @Then("^Register the customer with valid ([^\"]*), ([^\"]*), ([^\"]*), ([^\"]*) and other valid details in delivery page$")
    public void register_the_customer(String Firstname, String Surname, String HouseNumber, String PostCode) {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Agent_RegisterCustomerActions.PayGRegistration(Firstname, Surname, HouseNumber, PostCode);

        } catch (Exception e) { // TODO Auto-generated catch block
            log.debug("Unable to Register customer , please see the failure screenshot");
            Assert.fail("Unable to Register customer , please see the failure screenshot");

        }
    }


    @When("^Pay by card$")
    public void pay_by_card() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Thread.sleep(5000);
            Agent_RegisterCustomerActions.PaybyCard();
            Thread.sleep(15000);
            Agent_RegisterCustomerActions.CardDetails();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to Pay by card , please see the failure screenshot");
            Assert.fail("Unable to Pay by card , please see the failure screenshot");

        }
    }

    @When("^Pay by card for PAYM device$")
    public void pay_by_card_payn_device() {
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        try {
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Agent_RegisterCustomerActions.PaybyCard();
            Thread.sleep(15000);
            Agent_RegisterCustomerActions.CardDetails_PayM();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to Pay by card , please see the failure screenshot");
            Assert.fail("Unable to Pay by card , please see the failure screenshot");

        }
    }

    @When("^Pay by card for PAYM device for Existing customer$")
    public void pay_by_card_payn_device_new() {
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        try {
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Thread.sleep(15000);
            Agent_RegisterCustomerActions.PaybyCard_new();
            Thread.sleep(15000);
            Agent_RegisterCustomerActions.CardDetails_PayM();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to Pay by card , please see the failure screenshot");
            Assert.fail("Unable to Pay by card , please see the failure screenshot");

        }
    }

    @When("^submit order button is clicked$")
    public void submit_order_button_is_clicked() {
        try {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_ConfirmationPage.class);
            Agent_ConfirmationPageActions.SubmitOrder();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to get order confirmation screen , please see the failure screenshot");
            Assert.fail("Unable to get order confirmation screen , please see the failure screenshot");

        }
    }

    @Then("^Order confirmation message should be displayed$")
    public void order_confirmation_message_should_be_displayed() {
        try {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_ConfirmationPage.class);
            Agent_ConfirmationPageActions.Confirmationdetails();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to get order confirmation screen , please see the failure screenshot");
            Assert.fail("Unable to get order confirmation screen , please see the failure screenshot");

        }
    }

    //Then agent should be displayed with updated copy of 'Refer with Simo' response
    @Then("^agent should be displayed with updated copy of 'Refer with Simo' response$")
    public void agent_should_displayed_with_updated_copy_of_Refer_with_Sim0_response() {
        try {
            driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
            if (driver.findElements(By.xpath("//*[contains(text(),'2.co.uk')]")).size() > 0) {
                String str = driver.findElement(By.xpath("(//*[contains(text(),'2.co.uk')])[1]")).getText();
                log.debug("Agent should be displayed with updated copy of 'Refer with Simo' response as :" + str);

                Thread.sleep(3000);
            } else {
                log.debug("Failed to Agent should be displayed with updated copy of 'Refer with Simo' response ");

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Failed to Agent should be displayed with updated copy of 'Refer with Simo' response @ " + e.getStackTrace());
            Assert.fail("Failed to Agent should be displayed with updated copy of 'Refer with Simo' response @ " + e.getStackTrace());
        }
    }


    @Given("^Search for ([^\"]*) device$")
    public void search_for_Delayed_device(String Status) {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.SearchDevice(Status);
            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to Search device");
        }

    }


    //////////////////////////////////// CCA
    //////////////////////////////////// Agent///////////////////////////////////////////////////////

    @Given("^select a valid Handset and Tariff combination such that there is monthly$")
    public void select_a_valid_Handset_and_Tariff_combination_such_that_there_is_monthly() {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.CCAHandsetTariffCombination();
            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to select valid CCA tariff and handset combination");
        }
    }

    @Then("^Click on 'Generate CCA' button$")
    public void click_on_Generate_CCA_button() {

        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, O2RefreshDealSummaryPage.class);
            O2RefreshDealSummaryActions.DealSummarySectionforCCA();
            Thread.sleep(4000);
            O2RefreshDealSummaryActions.ClickGenerateCCABtn();

            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to enter the CCA email id");
        }
    }

    @Then("^click on the 'CCA' link$")
    public void click_on_the_CCA_link() {

        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, O2RefreshDealSummaryPage.class);
            O2RefreshDealSummaryActions.ClickGenerateCCALink();
            Thread.sleep(4000);
            O2RefreshDealSummaryActions.SwitchFocusToNewTab();

            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to enter the CCA email id");
        }

    }

    @Given("^Signin using valid ([^\"]*) and ([^\"]*) credentials$")
    public void signin_using_valid_ink_jun_and_test_credentials(String username, String password) {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, UpgradeCustomerPage.class);
            Thread.sleep(4000);
            UpgradeCustomerPageActions.Login(username, password);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.debug("Unable to signin using credentials");
            Assert.fail("Unable to signin using credentials");

        }
    }

    @Given("^clicks on 'Buyout' button$")
    public void clicks_on_Buyout_button() {
        // Write code here that turns the phrase above into concrete actions
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Thread.sleep(4000);
            Agent_DealBuilderPageActions.AgentBuyOut();
            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to click on Trade in Button");
        }

    }

    @And("^Confirm Device and Tariff are added in Deal Builder$")
    public void Confirm_Device_and_Tariff_are_added_in_DealBuilder() {
        try {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPage.deviceAdded_DealBuilder.isDisplayed();
            Thread.sleep(3000);
            Agent_DealBuilderPage.tariffAdded_DealBuilder.isDisplayed();
            Thread.sleep(1000);
            log.debug("Successfylly added Device & Tariff into Deal Builer");

        } catch (Exception e) {
            log.debug("Failed to add Device & Tariff into Deal Builer ");
            Assert.fail("Failed to add Device & Tariff into Deal Builer");
        }
    }


    @And("^select a valid store for Click and Collect")
    public void select_Store_for_click_and_Collect() {
        try {

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Thread.sleep(4000);
            Agent_DealBuilderPageActions.selectStore();

        } catch (Exception e) {
            log.debug("unable to select store");
            Assert.fail("unable to select store");
        }
    }

    @And("^Register customer with valid ([^\"]*), ([^\"]*), ([^\"]*) in delivery page$")
    public void Register_Customer_Phones(String Password, String confirmPassword, String SecurityAnswer) {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Agent_RegisterCustomerActions.RegisterCustomer(Password, confirmPassword, SecurityAnswer);
            Thread.sleep(5000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to Register customer");
        }
    }

    @And("^validate register status$")
    public void Validate_Register_Status() {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Thread.sleep(4000);
            Agent_RegisterCustomerActions.RegisterStatus();
            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Register status is miss matched");
        }
    }

    @Given("^select a valid Handset and Tariff combination for Phones$")
    public void select_a_valid_Handset_and_Tariff_combination_for_Phones() {
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.HandsetTariffCombinationforPhones();
            Thread.sleep(4000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Unable to select valid tariff and handset combination");
        }
    }

    /*
     * ##################################################################################
     *  Standard_or_CCA_targeted_bolton_promotion_tab_Agent_upgrade_options_page_Order_placement
     * #####################################################################################
     */
    @And("^Click on 'Select' Button in targeted promotion tab$")
    public void click_on_Select_Button_in_targeted_promotion_tab() {
        try {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.SelectPromotion();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to select button in targeted promotion tab , please see the failure screenshot");
            Assert.fail("Unable to get select button in targeted promotion tab , please see the failure screenshot");

        }
    }


    @And("^Click on 'Extras' tab$")
    public void click_on_Extras_tab() {
        try {

            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.ClickOnExtras();
            Thread.sleep(4000);
            log.debug("Successfully verified");
        } catch (Exception e) {
            log.debug("Unable to validate Extra section");
            Assert.fail("Unable to validate Extra section");
        }
    }


    @And("^Select a Bolton ([^\"]*)$")
    public void select_Bolton(String BoltonName) {
        try {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_DealBuilderPage.class);
            Agent_DealBuilderPageActions.SelectBolton(BoltonName);
            Thread.sleep(4000);
            log.debug("Successfully click and verfy the Bolton verified");
        } catch (Exception e) {
            log.debug("Unable to validate Bolton section");
            Assert.fail("Unable to validate Bolton section");
        }
    }


    @And("^Verify that the 'Checkout' CTA is not enabled$")
    public void verify_that_the_Checkout_CTA_is_not_enabled() {
        try {
            log.debug("Check out is vierified");
        } catch (Exception e) {
            log.debug("Unable to validate section");
            Assert.fail("Unable to validate section");
        }
    }

    @Then("^Choose HomeDelivery delivery address and delivery time$")
    public void HomeDelivery_Address() throws Throwable {
        log.debug("Choosing available delivery address");
        Thread.sleep(5000);
    }

    //GDPR preferences section for AFA  --- JamalKhan
    @Then("^Choose Business preferences ([^\"]*) ([^\"]*) ([^\"]*) ([^\"]*) and Channel Preferences ([^\"]*) ([^\"]*) ([^\"]*) ([^\"]*) for ([^\"]*) when GDPR ([^\"]*) ([^\"]*) ([^\"]*) for AFA journey$")
    public void Choose_Your_Preferences_AFA(String BP1, String BP2, String BP3, String BP4, String Chn1, String Chn2, String Chn3, String Chn4, String customer, String status, String DeviceType, String Device_Module) {
        // Write code here that turns the phrase above into concrete actions
        try {
            driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Thread.sleep(5000);
            Agent_RegisterCustomerActions.PreferencesSection_AFA(BP1, BP2, BP3, BP4, Chn1, Chn2, Chn3, Chn4, customer, status, DeviceType, Device_Module);
            log.debug("Completed preference actions");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to Choose your preferences, please see the failure screenshot");
            Assert.fail("Unable to Choose your preferences, please see the failure screenshot");

        }
    }

    //GDPR preferences section for AFU  --- JamalKhan
    @Then("^Choose Business preferences ([^\"]*) ([^\"]*) ([^\"]*) ([^\"]*) and Channel Preferences ([^\"]*) ([^\"]*) ([^\"]*) ([^\"]*) for ([^\"]*) when GDPR ([^\"]*) ([^\"]*) for AFU journey$")
    public void Choose_Your_Preferences_AFU(String BP1, String BP2, String BP3, String BP4, String Chn1, String Chn2, String Chn3, String Chn4, String customer, String status, String DeviceType) {
        // Write code here that turns the phrase above into concrete actions
        try {
            driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
            PageFactory.initElements(driver, Agent_RegisterCustomerPage.class);
            Thread.sleep(5000);
            Agent_RegisterCustomerActions.PreferencesSection_AFU(BP1, BP2, BP3, BP4, Chn1, Chn2, Chn3, Chn4, customer, status, DeviceType);
            log.debug("Completed preference actions");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.debug("Unable to Choose your preferences, please see the failure screenshot");
            Assert.fail("Unable to Choose your preferences, please see the failure screenshot");

        }
    }
}

             

 
